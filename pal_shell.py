#!/usr/bin/python -i
# coding=utf-8

## Generate Thrift code for Python
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/pal_rpc.thrift
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift

import sys, readline, rlcompleter

sys.path.append('./gen-py')

from pal_rpc import *
from pal_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

DEVICE = 0

transport = TSocket.TSocket('localhost', 9090)
transport = TTransport.TBufferedTransport(transport)

transport.open()

bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'pal')
client = pal.Client(protocol)

readline.parse_and_bind("tab: complete")
