#!/usr/bin/env python3
# coding=utf-8

## Generate Thrift code for Python
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/conn_mgr_pd_rpc.thrift

import sys
from scapy.all import *

sys.path.append('./gen-py')

from conn_mgr_pd_rpc import *
from conn_mgr_pd_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

DEVICE_ID = 0
DEVICE_PIPE_ID = 0
DEV_TGT = res_pd_rpc.ttypes.DevTarget_t(DEVICE_ID, DEVICE_PIPE_ID)
PKTGEN_SOURCE_PORT = 68
SESSION = 0
APP_ID = 0

transport = TSocket.TSocket('localhost', 9090)
transport = TTransport.TBufferedTransport(transport)
transport.open()

bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'conn_mgr')
client = conn_mgr.Client(protocol)

def init():
  client.pktgen_enable(SESSION, DEVICE_ID, PKTGEN_SOURCE_PORT)
  config = PktGenAppCfg_t()
  config.buffer_offset=0
  config.trigger_type = PktGenTriggerType_t.TIMER_PERIODIC
  config.src_port = PKTGEN_SOURCE_PORT
  return config

def stop_packets():
  client.pktgen_app_disable(SESSION, DEV_TGT, APP_ID)

def gen_packets(config, pps, length):
  pkt = Ether() / IP() / UDP(sport=55550,dport=55551)
  pkt = pkt / ("X" * (length - len(pkt)))
  config.length = len(pkt)
  client.pktgen_write_pkt_buffer(SESSION, DEV_TGT, 0, len(pkt), bytes(pkt))
  config.timer = 1000000000 // pps  # nanoseconds betweent packets
  client.pktgen_cfg_app(SESSION, DEV_TGT, APP_ID, config)
  client.pktgen_app_enable(SESSION, DEV_TGT, APP_ID)

config = init()
gen_packets(config, 1, 64) # Generates one 64 byte packet per second
