#!/usr/bin/python
# coding=utf-8

## Generate Thrift code for Python
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/pal_rpc.thrift
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift

import sys

sys.path.append('./gen-py')

from pal_rpc import *
from pal_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

DEVICE = 0

transport = TSocket.TSocket('localhost', 9090)
transport = TTransport.TBufferedTransport(transport)

transport.open()

bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'pal')
client = pal.Client(protocol)

def port_add(front_port, speed, fec_type):
    [port, lane] = front_port.split('/',1)
    device_port = client.pal_port_front_panel_port_to_dev_port_get(DEVICE, int(port),int(lane))
    client.pal_port_add(DEVICE, device_port, speed, fec_type)
    client.pal_port_enable(DEVICE, device_port)

# Enable uplink to SURF as 4 x 25G
port_add("32/0", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/1", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/2", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/3", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_REED_SOLOMON)

# Enable looped cable at 100G
port_add("20/0", pal_port_speed_t.BF_SPEED_100G, pal_fec_type_t.BF_FEC_TYP_NONE)
port_add("24/0", pal_port_speed_t.BF_SPEED_100G, pal_fec_type_t.BF_FEC_TYP_NONE)

# Enable links to UvA server (mc-b8)
port_add("16/0", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_NONE)
port_add("16/1", pal_port_speed_t.BF_SPEED_25G, pal_fec_type_t.BF_FEC_TYP_NONE)
