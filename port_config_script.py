# To be used with run_pd_rpc.py
# ./run_pd_rpc.py port_config_script.py

def port_add(front_port, speed, fec_type):
    [port, lane] = front_port.split('/',1)
    device_port = pal.port_front_panel_port_to_dev_port_get(int(port),int(lane))
    pal.port_add(device_port, speed, fec_type)
    pal.port_enable(device_port)

# Enable uplink to SURF as 4 x 25G
port_add("32/0", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/1", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/2", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_REED_SOLOMON)
port_add("32/3", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_REED_SOLOMON)

# Enable looped cable at 100G
port_add("20/0", pal.port_speed_t.BF_SPEED_100G, pal.fec_type_t.BF_FEC_TYP_NONE)
port_add("24/0", pal.port_speed_t.BF_SPEED_100G, pal.fec_type_t.BF_FEC_TYP_NONE)

# Enable links to UvA server (mc-b8)
port_add("16/0", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_NONE)
port_add("16/1", pal.port_speed_t.BF_SPEED_25G, pal.fec_type_t.BF_FEC_TYP_NONE)
