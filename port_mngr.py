#!/usr/bin/python
# coding=utf-8

## Generate Thrift code for Python
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/pal_rpc.thrift
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift

import sys, time

sys.path.append('./gen-py')

from pal_rpc import *
from pal_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

DEVICE = 0

transport = TSocket.TSocket('localhost', 9090)
transport = TTransport.TBufferedTransport(transport)

transport.open()

bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'pal')
client = pal.Client(protocol)

def init_ports(c):
    ports = {}
    for fp in range(1, 33):
        for lane in range(0, 4):
            dp = c.pal_port_front_panel_port_to_dev_port_get(DEVICE, fp, lane)
            if c.pal_port_is_valid(DEVICE, dp):
                # print("%d/%d (%d)" % (fp, lane, dp))
                ports[dp] = { 'name': ("%d/%d (%d)" % (fp, lane, dp)) }
    return ports

def init_stats(c, ports):
    for dp in ports:
        stats = c.pal_port_all_stats_get_with_ts(DEVICE, dp)
        ports[dp]['ts'] = stats.timestamp_s + stats.timestamp_ns/1000000000.0
        ports[dp]['frames'] = stats.entry[pal_rmon_counter_t.pal_mac_stat_FramesReceivedAll]
        ports[dp]['octets'] = stats.entry[pal_rmon_counter_t.pal_mac_stat_OctetsReceived]
        ports[dp]['drops'] = stats.entry[pal_rmon_counter_t.pal_mac_stat_FramesDroppedBufferFull]

def get_stats(c, ports):
    for dp in ports:
        stats = c.pal_port_all_stats_get_with_ts(DEVICE, dp)
        ts = stats.timestamp_s + stats.timestamp_ns/1000000000.0
        frames = stats.entry[pal_rmon_counter_t.pal_mac_stat_FramesReceivedAll]
        octets = stats.entry[pal_rmon_counter_t.pal_mac_stat_OctetsReceived]
        drops = stats.entry[pal_rmon_counter_t.pal_mac_stat_FramesDroppedBufferFull]

        d_ts = ts - ports[dp]['ts']
        d_octets = octets - ports[dp]['octets']
        d_frames = frames - ports[dp]['frames']
        d_drops = drops - ports[dp]['drops']
        print("%s %0.2f Bps (%0.2f pps, %0.2f drops/s)" % (ports[dp]['name'], d_octets/d_ts, d_frames/d_ts, d_drops/d_ts))

        ports[dp]['ts'] = ts
        ports[dp]['frames'] = frames
        ports[dp]['octets'] = octets
        ports[dp]['drops'] = drops

device_ports = init_ports(client)
init_stats(client, device_ports)

while True:
    time.sleep(2)
    get_stats(client, device_ports)
    print('')
