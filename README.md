
## Requirements

- SDE version => 9.5.0
- Thrift (=> 0.10.0 for Python 3 support)
- Python 2/3 Thrift library

## Generate Thrift code for Python

~~~
thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/ts_pd_rpc.thrift
thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift
thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/conn_mgr_pd_rpc.thrift
~~~
