#!/usr/bin/env python3
# coding=utf-8

import struct, sys
sys.path.append('./gen-py')

# from res_pd_rpc import *
# from res_pd_rpc.ttypes import *

from ts_pd_rpc import *
from ts_pd_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

def thrift_connect():
    transport = TSocket.TSocket('localhost', 9090)
    transport = TTransport.TBufferedTransport(transport)
    transport.open()

    bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
    protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'ts')
    return ts.Client(protocol)


c = thrift_connect()
print("Port 16/0 TS7: %d" % (c.ts_1588_timestamp_tx_get(0,0).ts))
print("Port 20/0 TS7: %d" % (c.ts_1588_timestamp_tx_get(0,28).ts))
print("Port 24/0 TS7: %d" % (c.ts_1588_timestamp_tx_get(0,60).ts))
