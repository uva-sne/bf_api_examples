#!/usr/bin/python3 -i
# coding=utf-8

## Generate Thrift code for Python
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/res.thrift
# thrift --gen py $SDE/pkgsrc/bf-drivers/pdfixed_thrift/thrift/conn_mgr_pd_rpc.thrift

import sys
#, readline, rlcompleter

sys.path.append('./gen-py')

from conn_mgr_pd_rpc import *
from conn_mgr_pd_rpc.ttypes import *

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TMultiplexedProtocol

DEVICE = 0

transport = TSocket.TSocket('localhost', 9090)
transport = TTransport.TBufferedTransport(transport)

transport.open()

bProtocol = TBinaryProtocol.TBinaryProtocol(transport)
protocol = TMultiplexedProtocol.TMultiplexedProtocol(bProtocol, 'conn_mgr')
client = conn_mgr.Client(protocol)

#readline.parse_and_bind("tab: complete")
